from functools import partial

from charm.toolbox.pairinggroup import G1, G2, GT, PairingGroup, pair, serialize
from charm.toolbox.symcrypto import SymmetricCryptoAbstraction
from charm.core.math.integer import randomBits
from charm.core.math.pairing import pairing

class Pairing:
    def __init__(self, curve, g1, g2):
        self.group = PairingGroup(curve)

        e = self.group
        self.g1 = e.deserialize(g1.encode())
        self.g2 = e.deserialize(g2.encode())
        self.h = partial(e.hash, type=G1)

    def pair(self, x, y):
        return pair(x, y)

    def deserialize(self, bytes):
        return self.group.deserialize(bytes)
