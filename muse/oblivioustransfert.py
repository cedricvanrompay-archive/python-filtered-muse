import random
from os import urandom
from math import ceil, sqrt
import json

from . import addhomocipher as ah
from .izgbf import IzgbfGeometry

ENDIANNESS = 'big'

GBF_SHARES_BYTE_LENGTH = 8

def all_equal(list):
    return len(set(list)) <= 1

class NotInlinePositionsError(Exception):
    pass

class DBDimensionError(Exception):
    pass

class DBNotSquareError(Exception):
    pass

class EncodedDB(list):
    pass

class OTQuery(list):
    pass

class OTResponse(ah.Ciphertext):
    def __init__(self, ciphertext):
        c = ciphertext
        super().__init__({'c': c['c']}, c.pk, c.key)

class SplitEncodedDB(list):
    pass

class SplitOTResponse(list):
    pass

class RecursiveEncodedDB(list):
    pass

class RecursiveOTQuery(list):
    pass

class RecursiveOTResponse(SplitOTResponse):
    pass

class OTQueryList(list):
    pass

class OTResponseList(list):
    pass

class Client:
    def __init__(self, ahcipher_params={}):
        self.cipher = ah.Cipher(**ahcipher_params)
        self.keygen()

    def keygen(self):
        (pk, sk) = self.cipher.keygen()

        self.public_key = pk
        self.secret_key = sk

    def get_public_material_bytes(self):
        return self.public_key.to_bytes()

    def open( self, response,
              message_byte_lenght=GBF_SHARES_BYTE_LENGTH,
              return_inner=False):

        max_db_byte_length = self.cipher.max_message_byte_length()

        if isinstance(response, RecursiveOTResponse):
            pk = self.public_key

            response = SplitOTResponse(response)

            inner_bytes = self.open(response, max_db_byte_length)
            if return_inner == 'bytes':
                return inner_bytes

            inner_response = OTResponse(ah.Ciphertext.from_bytes(inner_bytes, pk))
            return self.open(inner_response, message_byte_lenght)

        if isinstance(response, OTResponse):
            pk = self.public_key
            sk = self.secret_key
            decrypt = self.cipher.decrypt

            enc_msg =  decrypt(pk, sk, response)
            return int(enc_msg).to_bytes(message_byte_lenght, ENDIANNESS)

        elif isinstance(response, SplitOTResponse):
            splits = [ self.open(split, message_byte_lenght) for split in response ]
            return b''.join(splits)

        elif isinstance(response, OTResponseList):
            return [self.open(each, message_byte_lenght) for each in response]

    def query(self, target_positions, db_geometry):
        if isinstance(target_positions, list):
            return OTQueryList([
                self.query(position, db_geometry)
                for position in target_positions
            ])

        elif isinstance(target_positions, int):
            position = target_positions

            if isinstance(db_geometry, tuple):
                # to allow passing a bare tuple
                db_geometry = IzgbfGeometry(*db_geometry)

                row = position // db_geometry.width
                column = position % db_geometry.width 

                return RecursiveOTQuery([
                    self.query(target_positions=column,
                               db_geometry=db_geometry.width),
                    self.query(target_positions=row,
                               db_geometry=db_geometry.height),
                ])

            else:
                # non-recursive query
                pk = self.public_key
                encrypt = self.cipher.encrypt

                return OTQuery([
                    encrypt(pk, 1) if i==position
                    else encrypt(pk, 0)
                    for i in range(db_geometry)
                ])

class Server:
    def __init__(self, public_key, ahcipher_params={}):
        if isinstance(public_key, bytes):
            public_key = ah.PublicKey.from_bytes(public_key)
        self.public_key = public_key
        self.cipher = ah.Cipher.from_key(public_key, **ahcipher_params)

    def encode(self, data, db_geometry=None):
        max_db_byte_length = self.cipher.max_message_byte_length()

        # XXX assuming all DB components have same byte length
        component_bytelenght = len(data[0])
        nb_splits = ceil(component_bytelenght / max_db_byte_length)

        if nb_splits == 1:
            if db_geometry == None:
                return EncodedDB([
                    int.from_bytes(x, byteorder=ENDIANNESS)
                    for x in data
                ])
            else:
                # XXX
                assert len(db_geometry) == 2
                db_geometry = IzgbfGeometry(*db_geometry) 
                w = db_geometry.width
                return RecursiveEncodedDB([
                    self.encode(data[i:i+w]) for i in range(0, len(data), w)
                ])

        else:
            n = max_db_byte_length

            result = SplitEncodedDB()
            for i in range(0, len(data[0]), n):
                split = [ x[i:i+n] for x in data]
                result.append(self.encode(split))

            return result


    def apply(self, query, encoded_database, send_intermediate_db=False):
        enc_db = encoded_database

        if isinstance(query, OTQueryList):
            return OTResponseList([
                self.apply(each, enc_db)
                for each in query
            ])

        elif isinstance(query, RecursiveOTQuery):
            if not isinstance(enc_db, RecursiveEncodedDB):
                raise RuntimeError("enc_db is not of type RecursiveEncodedDB"
                                   "but of type %s" % type(enc_db))

            tmp_db = [
                self.apply(query[0], row)
                for row in enc_db
            ]

            if send_intermediate_db == 'encrypted':
                return OTResponseList(tmp_db)

            tmp_db_bytes = [
                each.to_bytes()
                for each in tmp_db
            ]

            if send_intermediate_db == 'bytes':
                return tmp_db_bytes

            enc_tmp_db = self.encode(tmp_db_bytes)

            final_response = self.apply(query[1], enc_tmp_db)
            return RecursiveOTResponse(final_response)

        elif isinstance(query, OTQuery):
            if isinstance(enc_db, EncodedDB):
                zero = self.cipher.encrypt(self.public_key, 0)
                zipped = zip(query, enc_db)
                return OTResponse(sum((x*y for (x,y) in zipped), zero))

            elif isinstance(enc_db, SplitEncodedDB):
                return SplitOTResponse([
                    self.apply(query, db_split)
                    for db_split in enc_db
                ])
            else:
                raise RuntimeError("bad type for encoded db: %s" % type(enc_db))

        else:
            raise RuntimeError("bad type for query: %s" % type(query))

