import json

from . import core
from . import networking
from .serialization import serialize, deserialize

class Reader:
    def __init__(self, name):
        self.name = name

        with open('muse.parameters.json') as parameter_file:
            parameters = json.load(parameter_file)

        self.netw_mgr = networking.NetworkManager(**parameters['networking'])

        self.netw_mgr.wait_for_server_to_be_ready('DH')
        self.netw_mgr.wait_for_server_to_be_ready('QM')

        core_parameters = deserialize(self.netw_mgr.fetch_core_params(), core.Parameters)
        self._core_reader = core.Reader(core_parameters, self.name)
        
        self.netw_mgr.send_transmission_key(self._core_reader.transmission_key,
                                         self.name)
        self.send_public_key()

    def send_public_key(self):
        data = serialize(self._core_reader.public_key)
        self.netw_mgr.send_reader_public_key(data, reader_name=self.name)

    def search(self, keyword):
        trapdoor, blinding_factor = self._core_reader.trapdoor(keyword.encode())

        self.netw_mgr.send_blinding_factor(
                data=serialize(blinding_factor),
                reader_name=self.name
        )

        filtered_response_data = self.netw_mgr.send_trapdoor(
                data=serialize(trapdoor),
                reader_name=self.name
        )
        filtered_response = deserialize(filtered_response_data,
                                        core.FilteredResponse)

        result = [self._core_reader.open(each) for each in filtered_response]
        return result
