from random import SystemRandom
from os import urandom
from hashlib import md5
from functools import reduce
from operator import xor
from collections import namedtuple
from math import ceil, log, sqrt

# XXX get it dynamically
OT_CIPHER_EXPANSION_FACTOR = 4

IzgbfGeometry = namedtuple('IzgbfGeometry',
                           ['width', 'height'])

class InsertionFailedError(Exception):
    pass

class FalseNegativeError(Exception):
    pass

class IzgbfArray(list):
    pass

class IZGBF:
    def __init__(self, nb_hash_fns=4, component_bytesize=8, false_negative_probability=10**-8):
        # we prefer lowering the number of hash function and increasing the filter size
        # because our cost is linear with the number of hash functions
        # but only linear with the square root of the total filter size
        self.nb_hash_fns = nb_hash_fns
        self.component_bytesize = component_bytesize

        # XXX actually this is the probability that
        # you are unable to insert the last element
        # https://gitlab.eurecom.fr/cedric.van-rompay/python-filtered-muse/issues/4
        self.false_negative_probability = false_negative_probability

    def compute_geometry(self, nb_elements):
        '''see https://gitlab.eurecom.fr/cedric.van-rompay/python-filtered-muse/issues/4'''
        eta = self.nb_hash_fns
        n = nb_elements
        f = self.false_negative_probability

        # m is the total number of components in the IZGBF
        # actually this will be not be exactly the case since
        # the effective number will be width*height
        m = ceil( (-eta*n) / log(1 - f**(1/n)) )

        F = OT_CIPHER_EXPANSION_FACTOR

        width = ceil(sqrt(m*F))
        height = ceil(m/width)

        # an adjustment because rows do not get exactly n/h elements,
        # this is only the expectation, and if they get more than what they are calibrated for
        # we get insertion errors
        width = int(width*1.3)

        return IzgbfGeometry(width, height)

    def _map(self, x, geometry):
        eta = self.nb_hash_fns
        width, height = geometry

        digest = md5(x).digest()
        line = int.from_bytes(digest, byteorder='little') % height
        positions = list()
        for _ in range(eta):
            digest = md5(digest).digest()
            line_position = int.from_bytes(digest, byteorder='little') % width
            positions.append(line*height + line_position)

        return list(set(positions))

    def map(self, x, nb_elements):
        geometry = self.compute_geometry(nb_elements)
        return self._map(x, geometry)

    @staticmethod
    def combine(shares):
        shares = (int.from_bytes(share, byteorder='little') for share in shares)
        share_sum = reduce(xor, shares)

        return share_sum

    @classmethod
    def check(cls, shares):
        if cls.combine(shares) == 0:
            return True
        else:
            return False

    def build(self, input_set, allow_failures=True):
        eta = self.nb_hash_fns
        geometry = self.compute_geometry(len(input_set))
        width, height = geometry
        l = self.component_bytesize

        size = height*width
        getrandbits = SystemRandom().getrandbits

        components = IzgbfArray([ None for _ in range(size) ])
        components.geometry = geometry

        for x in input_set:
            empty_slot = None
            final_share = 0

            positions = self._map(x, geometry)

            for i in positions:
                if components[i] == None:
                    if empty_slot == None:
                        # reserve location for final share
                        empty_slot = i
                    else:
                        share = getrandbits(8*l)
                        components[i] = share
                        final_share ^= share
                else:
                    # print('IZGBF: used slot (element {}, position {})'.format(str(x)[3:8], i))
                    final_share ^= components[i]

            if empty_slot == None:
                if not allow_failures:
                    raise InsertionFailedError()
            else:
                components[empty_slot] = final_share
                assert reduce(xor, [components[i] for i in positions]) == 0

        for (i,share) in enumerate(components):
            if share == None:
                components[i] = urandom(l)
            elif isinstance(share, int):
                components[i] = share.to_bytes(l, byteorder='little')

        return components
