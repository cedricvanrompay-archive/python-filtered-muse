import logging
from binascii import hexlify
import json

from flask import Flask, request, abort, render_template
app = Flask(__name__)

from .serialization import serialize, deserialize
from . import networking
from . import core

# TODO remove
logger = logging.getLogger('simple_example')
logger.setLevel(logging.DEBUG)

with open('muse.parameters.json') as parameter_file:
    parameters = json.load(parameter_file)

netw_mgr = networking.NetworkManager(**parameters['networking'])

netw_mgr.wait_for_server_to_be_ready('DH')


core_parameters = deserialize(netw_mgr.fetch_core_params(), core.Parameters)
qm = core.QueryMultiplexer(core_parameters,
                           parameters.get('addhomocipher', {}))

netw_mgr.send_ot_public_material(qm.get_ot_public_material())

reader_public_keys = dict()
authorizations = dict()
indexes_length = dict()

@app.route('/')
def hello():
    sections = [
        {
            'title': 'Reader Public Keys',
            'field_names': ['Reader ID', 'public key'],
            'records': [
                (reader_id, hexlify(pk).decode().upper())
                for (reader_id, pk) in reader_public_keys.items()
            ]
            
        },
        {
            'title': 'Authorizations',
            'field_names': ['Reader ID', 'Writer ID'],
            'records': [
                (reader_id, writer_id)
                for reader_id in authorizations
                for writer_id in authorizations[reader_id]
            ]
        }
    ]

    return render_template('dashboard.template.html',
                           entity_name='Query Multiplexer',
                           navbar_type='navbar-dark bg-info',
                           sections=sections)

@app.route('/reader_public_keys/<reader_name>', methods=['GET', 'POST'])
def reader_public_key(reader_name):
    if request.method == 'GET':
        if reader_name in reader_public_keys:
            return reader_public_keys[reader_name]
        else:
            abort(404)
    elif request.method == 'POST':
        reader_public_keys[reader_name] = request.data
        return 'set public key of reader %s' % reader_name

@app.route('/authorizations/<reader_name>/<writer_name>', methods=['GET', 'POST'])
def authorization(reader_name, writer_name):
    if request.method == 'GET':
        if reader_name in authorizations:
            if writer_name in authorizations[reader_name]:
                return authorizations[reader_name][writer_name]
            else:
                abort(404)
        else:
            abort(404)
    elif request.method == 'POST':
        if reader_name not in authorizations:
            authorizations[reader_name] = dict()
        auth = deserialize(request.data, core.Authorization,
                           pairing_group=qm.pairing.group)
        authorizations[reader_name][writer_name] = auth
        return ('set authorization for reader %s to search index of writer %s'
                % (reader_name, writer_name))

@app.route('/indexes_length/<index_id>', methods=['POST'])
def set_index_length(index_id):
    length = int(request.data)
    indexes_length[index_id] = length
    return 'set length of index %s to %i' % (index_id, length)

@app.route('/trapdoor/<reader_name>', methods=['POST'])
def transform_and_filter(reader_name):
    if reader_name not in authorizations:
        return ('no authorizations for reader %s' % reader_name, 403)

    else:
        trapdoor = deserialize(request.data, core.ReaderTrapdoor,
                               pairing_group=qm.pairing.group)

        filtered_responses = core.FilteredResponse()

        for (writer_name,auth) in authorizations[reader_name].items():
            transformed_trapdoor = qm.transform(trapdoor, auth,
                                                indexes_length[writer_name])

            received = netw_mgr.send_transformed_trapdoor(
                    data=serialize(transformed_trapdoor),
                    reader_name=reader_name,
                    index_id=writer_name
            )
            
            response = deserialize(received, core.Response,
                                   recursive=True, response_list=True,
                                   public_key=qm.ot_client.public_key)

            filtered_response = qm.filter(response)
            if filtered_response != None:
                filtered_responses.append(filtered_response)

        return serialize(filtered_responses)



def main():
    port = parameters['networking']['qm_address']['port']
    app.run(port=port)

if __name__ == "__main__":
    main()