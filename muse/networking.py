import requests
from time import sleep

import requests as rq

from .serialization import serialize, deserialize
from . import core

class ServerUnreachableError(Exception):
    pass

# define our own 'post' and 'get'
# just so that we don't have to repeat
# "raise_for_status" every time

class NetworkManager:
    def __init__(self, dh_address, qm_address):
        self.dh_address = "http://%s:%i" % (dh_address['ip'], dh_address['port'])
        self.qm_address = "http://%s:%i" % (qm_address['ip'], qm_address['port'])

    def post(self, *args, **kwargs):
        response = rq.post(*args, **kwargs)
        response.raise_for_status()
        return response

    def get(self, *args, **kwargs):
        response = rq.get(*args, **kwargs)
        response.raise_for_status()
        return response

    def wait_for_server_to_be_ready(self, who):
        if who.upper() == 'QM':
            address = self.qm_address
        elif who.upper() == 'DH':
            address = self.dh_address
        else:
            raise ValueError('unknown entity "%s"; must be "QM" or "DH".' % who)

        for i in range(5):
            try:
                requests.get(address)
            except requests.exceptions.ConnectionError as e:
                sleep(1)
                continue
            break
        else:
            raise ServerUnreachableError('cannot connect to %s (too many attempts)' % address)

    def fetch_core_params(self):
        response = self.get(self.dh_address+'/' + 'parameters')
        return response.content

    def send_transmission_key(self, data, reader_name):
        address = self.dh_address+'/'+'transmission_keys'+'/'+reader_name
        self.post(address, data=data)

    def send_ot_public_material(self, data):
        address = '/'.join([self.dh_address, 'ot_public_material'])
        self.post(address, data=data)

    def send_reader_public_key(self, data, reader_name):
        address = '/'.join([self.qm_address, 'reader_public_keys', reader_name])
        self.post(address, data=data)

    def send_encrypted_index(self, data, writer_name):
        address = self.dh_address+'/'+'encrypted_indexes'+'/'+writer_name
        self.post(address, data=data)

    def send_index_length(self, length, index_id):
        address = self.qm_address+'/'+'indexes_length'+'/'+index_id
        self.post(address, json=length)

    def fetch_reader_pubkey(self, reader_name):
        address = self.qm_address+'/'+'reader_public_keys'+'/'+reader_name
        response = self.get(address)
        return response.content

    def send_auth(self, data, reader_name, writer_name):
        address = '/'.join([self.qm_address, 'authorizations', reader_name, writer_name])
        self.post(address, data=data)

    def send_blinding_factor(self, data, reader_name):
        address = self.dh_address+'/'+'blinding_factor'+'/'+reader_name
        self.post(address, data=data)

    def send_trapdoor(self, data, reader_name):
        address = self.qm_address+'/'+'trapdoor'+'/'+reader_name
        response = self.post(address, data=data)
        return response.content

    def send_transformed_trapdoor(self, data, reader_name, index_id):
        address = self.dh_address+'/'+'transformed_trapdoor'+'/'+reader_name+'/'+index_id
        response = self.post(address, data)
        return response.content
