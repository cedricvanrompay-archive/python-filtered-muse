import json
from base64 import b64encode, b64decode

from . import core, pairing
from .core import (
        Authorization, EncryptedIndex, ReaderTrapdoor,
        BlindingFactor, TransformedTrapdoor, Response,
        Parameters, ReaderPublicKey, FilteredResponse
                  )
from . import oblivioustransfert as ot
from . import addhomocipher as ah

def serialize(x):
    if isinstance(x, Parameters):
        return json.dumps(x).encode()

    elif isinstance(x, Authorization):
        return json.dumps((x.reader_id,
                           x.index_id,
                           pairing.serialize(x.value).decode(),
        )).encode()

    elif isinstance(x, ReaderPublicKey):
        return json.dumps((x.reader_id,
                          pairing.serialize(x.value).decode(),
        )).encode()

    elif isinstance(x, EncryptedIndex):
        dump = json.dumps([pairing.serialize(each).decode() for each in x])
        return dump.encode()

    elif isinstance(x, ReaderTrapdoor):
        dump = json.dumps((pairing.serialize(x.value).decode(),
                           x.reader_id))
        return dump.encode()

    elif isinstance(x, BlindingFactor):
        return pairing.serialize(x.value)

    elif isinstance(x, TransformedTrapdoor):
        dump = json.dumps((serialize(x.query),
                           x.reader_id,
                           x.index_id))
        return dump.encode()

    elif isinstance(x, ot.OTQuery):
        return [ b64encode(ctxt.to_bytes()).decode()
                 for ctxt in x ]

    elif isinstance(x, ot.RecursiveOTQuery):
        return [ serialize(subquery) for subquery in x ]

    elif isinstance(x, ot.OTQueryList):
        return [ serialize(query) for query in x ]

    elif isinstance(x, Response):
        dump = json.dumps((serialize(x.value), b64encode(x.encrypted_index_id).decode()))
        return dump.encode()

    elif isinstance(x, ot.OTResponse):
        return b64encode(x.to_bytes()).decode()

    elif isinstance(x,
                    (
                        ot.SplitOTResponse,
                        ot.RecursiveOTResponse,
                        ot.OTResponseList
                    )):
        return [ serialize(each) for each in x ]

    elif isinstance(x, FilteredResponse):
        dump = json.dumps([ b64encode(each).decode() for each in x])
        return dump.encode()

    else:
        raise ValueError("cannot serialize type %s" % type(x))

def deserialize(data, output_type, **kwargs):
    if output_type == Parameters:
        return Parameters(*json.loads(data.decode()))

    elif output_type == Authorization:
        assert 'pairing_group' in kwargs
        pairing_group = kwargs['pairing_group']

        t = json.loads(data.decode())
        return Authorization(t[0], t[1],
                                  pairing_group.deserialize(t[2].encode()))

    elif output_type == ReaderPublicKey:
        assert 'pairing_group' in kwargs
        pairing_group = kwargs['pairing_group']

        t = json.loads(data.decode())
        return ReaderPublicKey(t[0],
                               pairing_group.deserialize(t[1].encode()))

    elif output_type == EncryptedIndex:
        assert 'pairing_group' in kwargs
        pairing_group = kwargs['pairing_group']
        return EncryptedIndex([
            pairing_group.deserialize(each.encode())
            for each in json.loads(data.decode())
        ])

    elif output_type == ReaderTrapdoor:
        assert 'pairing_group' in kwargs
        pairing_group = kwargs['pairing_group']
        t = json.loads(data.decode())
        return ReaderTrapdoor(pairing_group.deserialize(t[0].encode()),
                              t[1])

    elif output_type == BlindingFactor:
        assert 'pairing_group' in kwargs
        pairing_group = kwargs['pairing_group']
        return BlindingFactor(pairing_group.deserialize(data))

    elif output_type == TransformedTrapdoor:
        assert 'public_key' in kwargs
        public_key = kwargs['public_key']

        recursive = kwargs['recursive']
        query_list = kwargs['query_list']
        if query_list:
            query_type = ot.OTQueryList
        elif recursive:
            query_type = ot.RecursiveOTQuery
        else:
            query_type = ot.OTQuery

        t = json.loads(data.decode())
        # note that 'recursive' keyword argument may be useless
        # in case query_type is not ot.OTQueryList
        # but that's fine
        query = deserialize(t[0], query_type,
                            recursive=recursive, public_key=public_key)
        return TransformedTrapdoor(query, t[1], t[2]) 

    elif output_type == ot.OTQuery:
        assert 'public_key' in kwargs
        public_key = kwargs['public_key']

        return ot.OTQuery([
            ah.Ciphertext.from_bytes(b64decode(each), public_key)
            for each in data
        ])

    elif output_type == ot.RecursiveOTQuery:
        assert 'public_key' in kwargs
        public_key = kwargs['public_key']

        return ot.RecursiveOTQuery([
            deserialize(each, ot.OTQuery, public_key=public_key)
            for each in data
        ])

    elif output_type == ot.OTQueryList:
        assert 'public_key' in kwargs
        public_key = kwargs['public_key']

        recursive = kwargs['recursive']
        if recursive:
            query_type = ot.RecursiveOTQuery
        else:
            query_type = ot.OTQuery

        return ot.OTQueryList([
            deserialize(each, query_type, public_key=public_key)
            for each in data
        ])

    elif output_type == Response:
        assert 'public_key' in kwargs
        public_key = kwargs['public_key']
        
        recursive = kwargs['recursive']
        response_list = kwargs['response_list']
        if response_list:
            response_type = ot.OTResponseList
        elif recursive:
            response_type = ot.RecursiveOTResponse
        else:
            response_type = ot.OTResponse

        t = json.loads(data.decode())
        return Response(
                deserialize(t[0], response_type,
                            public_key=public_key,
                            recursive=recursive),
                b64decode(t[1])
        )

    elif output_type == ot.OTResponse:
        assert 'public_key' in kwargs
        public_key = kwargs['public_key']

        return ot.OTResponse(
            ah.Ciphertext.from_bytes(b64decode(data), public_key)
        )

    elif output_type in (ot.SplitOTResponse, ot.RecursiveOTResponse):
        return output_type([
            deserialize(each, ot.OTResponse, public_key=kwargs['public_key'])
            for each in data
        ])

    elif output_type == ot.OTResponseList:
        if kwargs['recursive']:
            response_type = ot.RecursiveOTResponse
        else:
            response_type = ot.OTResponse

        return ot.OTResponseList([
            deserialize(each, response_type, public_key=kwargs['public_key'])
            for each in data
        ])

    elif output_type == core.FilteredResponse:
        l = json.loads(data.decode())
        return [b64decode(each) for each in l]


