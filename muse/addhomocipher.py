'''An additively homomorphic encryption scheme
implemented with the Pailler Crytosystem from Charm Crypto [1]
to which we added serialization methods

[1] https://github.com/JHUISI/charm/blob/dev/charm/schemes/pkenc/pkenc_paillier99.py
'''
from math import log2, floor
import json

from charm.toolbox.integergroup import RSAGroup
import charm.schemes.pkenc.pkenc_paillier99
from charm.core.math.integer import integer

ENDIANNESS = 'big'

class Ciphertext(charm.schemes.pkenc.pkenc_paillier99.Ciphertext):
    def to_bytes(self):
        bytelength = self.pk['ciphertext_bytelength']
        return int(self['c']).to_bytes(bytelength, ENDIANNESS)

    @classmethod
    def from_bytes(cls, bytes, public_key):
        c = int.from_bytes(bytes, ENDIANNESS) % public_key['n2']
        return cls({'c': c}, public_key, 'c')

class PublicKey(dict):
    def to_bytes(self):
        return json.dumps({
            k:int(v)
            for (k,v) in self.items()
        }).encode()

    @classmethod
    def from_bytes(Class, bytes):
        d = json.loads(bytes.decode())

        result = Class({
            'g': integer(d['g']) % integer(d['n2']),
            'n': integer(d['n']),
            'n2': integer(d['n2']),
            'ciphertext_bytelength': d['ciphertext_bytelength']
        })

        return result

class Cipher(charm.schemes.pkenc.pkenc_paillier99.Pai99):
    def __init__(self, group=None, secparam=1024):
        self.group = group or RSAGroup()
        self.secparam = secparam
        super().__init__(self.group)

    def encrypt(self, public_key, message):
        pk = public_key
        m = message

        c = super().encrypt(pk, m)['c']

        return Ciphertext({'c': c}, pk, 'c')

    @classmethod
    def from_key(Class, key, secparam=None):
        group = RSAGroup()
        group.n = key['n']
        if secparam:
            return Class(group, secparam)
        else:
            return Class(group)

    def keygen(self):
        pk, sk = super().keygen(self.secparam)
        pk = PublicKey(pk)
        pk['ciphertext_bytelength'] = (self.secparam*4) // 8

        return pk, sk

    def max_message_byte_length(self):
        return ( (self.secparam*2) // 8 ) // 2

def get_max_msg_bitlength(public_key):
    n = public_key['n']
    return floor(log2(n))
