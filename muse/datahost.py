import json

from flask import Flask, request, abort, render_template
app = Flask(__name__)

from . import core
from .serialization import serialize, deserialize
from . import networking

with open('muse.parameters.json') as parameter_file:
    parameters = json.load(parameter_file)

netw_mgr = networking.NetworkManager(**parameters['networking'])

core_parameters = core.param_gen()
dh = core.DataHost(core_parameters,
                   parameters.get('addhomocipher', {}))

encrypted_indexes = dict()
blinding_factors = dict()

@app.route('/')
def hello():
    sections = [
        {
            'title': 'Encrypted Indexes',
            'field_names': ['Index ID', 'number of keywords'],
            'records': [
                (index_id, len(enc_idx))
                for (index_id, enc_idx) in encrypted_indexes.items() 
            ]
        }
    ]

    return render_template('dashboard.template.html',
                           entity_name='Data Host',
                           navbar_type='navbar-dark bg-success',
                           sections=sections)

@app.route("/parameters")
def send_parameters():
    return serialize(core_parameters)

@app.route('/ot_public_material', methods=['POST'])
def set_ot_public_material():
    dh.set_ot(request.data)
    return 'set OT public material'

@app.route('/transmission_keys/<reader_id>', methods=['POST'])
def set_transmission_key(reader_id):
    dh.update_transmission_key(reader_id, request.data)
    return 'set transmission key of reader %s' % reader_id

@app.route('/encrypted_indexes/<writer_id>', methods=['POST'])
def set_encrypted_index(writer_id):
    enc_idx = deserialize(request.data,
                          core.EncryptedIndex,
                          pairing_group=dh.pairing.group)

    encrypted_indexes[writer_id] = enc_idx
    netw_mgr.send_index_length(len(enc_idx), writer_id)

    return 'set encrypted index of writer %s' % writer_id

@app.route('/blinding_factor/<reader_name>', methods=['POST'])
def set_blinding_factor(reader_name):
    blinding_factors[reader_name] = deserialize(request.data,
                                                core.BlindingFactor,
                                                pairing_group=dh.pairing.group)

    return 'set blinding factor for reader %s' % reader_name

@app.route('/transformed_trapdoor/<reader_name>/<index_id>', methods=['POST'])
def process(reader_name, index_id):
    encrypted_index = encrypted_indexes[index_id]
    blinding_factor = blinding_factors[reader_name]

    transformed_trapdoor = deserialize(request.data, core.TransformedTrapdoor,
                                       recursive=True, query_list=True,
                                       public_key=dh.ot_server.public_key)

    prepared_index = dh.prepare(encrypted_index, blinding_factor)

    response = dh.process(transformed_trapdoor, prepared_index)

    return serialize(response)
    

def main():
    port = parameters['networking']['dh_address']['port']
    app.run(port=port)


if __name__ == "__main__":
    main()