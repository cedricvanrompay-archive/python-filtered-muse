import json

from . import core
from . import networking
from .serialization import serialize, deserialize

class Writer:
    def __init__(self, name):
        self.name = name

        with open('muse.parameters.json') as parameter_file:
            parameters = json.load(parameter_file)

        self.netw_mgr = networking.NetworkManager(**parameters['networking'])

        self.netw_mgr.wait_for_server_to_be_ready('DH')
        self.netw_mgr.wait_for_server_to_be_ready('QM')

        core_parameters = deserialize(self.netw_mgr.fetch_core_params(), core.Parameters)
        self._core_writer = core.Writer(core_parameters, self.name)
        
    def encrypt(self, path_to_index):
        with open(path_to_index) as index_file:
            index = [line.strip().encode() for line in index_file.readlines()]
            encrypted_index = self._core_writer.encrypt(index)
            data = serialize(encrypted_index)
            self.netw_mgr.send_encrypted_index(data, writer_name=self.name)
    
    def delegate(self, reader_name):
        pubkey = deserialize(self.netw_mgr.fetch_reader_pubkey(reader_name),
                             core.ReaderPublicKey,
                             pairing_group=self._core_writer.pairing.group)
        auth = self._core_writer.delegate(pubkey)
        data = serialize(auth)
        self.netw_mgr.send_auth(data, reader_name, writer_name=self.name)
