from collections import namedtuple
from types import GeneratorType
import json
from functools import partial

from .pairing import Pairing
from . import oblivioustransfert as ot
from .izgbf import IZGBF

Parameters = namedtuple('Parameters',
                        ['curve', 'g1', 'g2'])

def param_gen(
        curve='MNT159',
        g1='1:Ih0YeEA/J1XidlBPkiMAvuy1Z4QB',
        g2='2:YfbY37T5PeaP6aNKhAbOl29J/j1LMPusi9PoVGg5BHBEXNwwH60utGkeurAccnEB5/sS6ehdxwsSOlROAA=='):

    return Parameters(curve, g1, g2)


class TransmissionCipher:
    def __init__(self, key):
        pass

    def encrypt(self, message):
        if isinstance(message, str):
            message = message.encode()
        return message

    def decrypt(self, ciphertext):
        return ciphertext.decode()

Authorization = namedtuple('Authorization',
                           ['reader_id', 'index_id', 'value'])

ReaderPublicKey = namedtuple('ReaderPublicKey',
                             ['reader_id', 'value'])

class EncryptedIndex(list):
    pass

class Writer:
    def __init__(self, parameters, index_id):
        self.parameters = parameters
        self.pairing = Pairing(parameters.curve, parameters.g1, parameters.g2)
        self.keygen()
        self.index_id = index_id

    def keygen(self):
        e = self.pairing.group
        self.secret_key =  e.random()

    def encrypt(self, data):
        if not isinstance(data, (list, tuple, GeneratorType)):
            raise ValueError('data must be an list, tuple or generator')

        pair = self.pairing.pair
        h = self.pairing.h
        gamma = self.secret_key
        g2 = self.pairing.g2
        x = data

        return EncryptedIndex([pair(h(x) ** gamma, g2) for x in data])

    def delegate(self, reader_pubkey):
        rho = reader_pubkey.value
        gamma = self.secret_key

        return Authorization(
            reader_pubkey.reader_id,
            self.index_id,
            value=rho**gamma
        )

ReaderTrapdoor = namedtuple('ReaderTrapdoor',
                            ['value', 'reader_id']
                            )

class BlindingFactor(namedtuple('BlindingFactor',
                     ['value'])):
    pass

class Reader:
    def __init__(self, parameters, id):
        self.parameters = parameters
        self.pairing = Pairing(parameters.curve, parameters.g1, parameters.g2)
        self.id = id
        self.keygen()
    
    def keygen(self):
        e = self.pairing.group
        g2 = self.pairing.g2

        rho = e.random()

        self.secret_key = rho

        pubkey_value = g2**(~rho)
        self.public_key = ReaderPublicKey(self.id, pubkey_value)

        self.transmission_key = None

    def trapdoor(self, word):
        h = self.pairing.h
        e = self.pairing.group
        rho = self.secret_key

        xi = e.random()
        t = h(word)**(xi * rho)

        return (ReaderTrapdoor(t, self.id), BlindingFactor(xi))

    def open(self, filtered_response):
        k_r = self.transmission_key
        return TransmissionCipher(key = k_r).decrypt(filtered_response)

TransformedTrapdoor = namedtuple('TransformedTrapdoor',
                                 ['query', 'reader_id', 'index_id'])

class FilteredResponse(list):
    pass

class QueryMultiplexer:
    def __init__(self, parameters, ah_parameters={}):
        self.parameters = parameters
        self.ot_client = ot.Client(ah_parameters)
        self.pairing = Pairing(parameters.curve, parameters.g1, parameters.g2)
        self.izgbf = IZGBF()

    def get_ot_public_material(self):
        return self.ot_client.get_public_material_bytes()

    def transform(self, trapdoor, authorization, index_length):
        e = self.pairing.group
        w = authorization.index_id
        r = trapdoor.reader_id
        delta = authorization.value
        t = trapdoor.value
        pair = self.pairing.pair

        c = pair(t, delta)
        c = e.serialize(c)
        positions = self.izgbf.map(c, index_length)
        geometry = self.izgbf.compute_geometry(index_length)
        query = self.ot_client.query(positions, geometry)

        return TransformedTrapdoor(query, r, w)

    def filter(self, response):
        shares = self.ot_client.open(response.value)
        if self.izgbf.check(shares):
            return response.encrypted_index_id
        else:
            return None

Response = namedtuple('Response',
                      ['value', 'encrypted_index_id'])

class DataHost:
    def __init__(self, parameters, ah_parameters={}):
        self.parameters = parameters
        self.ah_parameters = ah_parameters
        self.transmission_keys = dict()
        self.pairing = Pairing(parameters.curve, parameters.g1, parameters.g2)
        self.izgbf = IZGBF()

    def set_ot(self, ot_public_material):
        pk = ot_public_material
        self.ot_server = ot.Server(pk, self.ah_parameters)

    def update_transmission_key(self, reader_id, key):
        self.transmission_keys.update({reader_id: key})

    def prepare(self, encrypted_index, blinding_factor, recursive=True):
        e = self.pairing.group
        xi = blinding_factor.value

        enc_idx_xi = [ e.serialize(x**xi) for x in encrypted_index ]
        b = self.izgbf.build(enc_idx_xi)
        geometry = self.izgbf.compute_geometry(len(encrypted_index))
        encoded_filter = self.ot_server.encode(b, geometry)

        return encoded_filter

    def process(self, transformed_trapdoor, encoded_database):
        query = transformed_trapdoor.query
        w = transformed_trapdoor.index_id
        r = transformed_trapdoor.reader_id
        
        response_value = self.ot_server.apply(query, encoded_database)

        k_r = self.transmission_keys[r]
        enc_w = TransmissionCipher(key=k_r).encrypt(w)

        return Response(response_value, enc_w)
