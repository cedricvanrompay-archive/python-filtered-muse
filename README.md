# Setup

We recommend that you **use a Virtual Machine**,
because the setup process installs some libraries system-wide.

Setup is done the following way:

```
sudo bash setup.sh
```

# Test

```
python3 test.py
```

`test_izgbf` fails sometimes, it's okay. What should really be tsted is that it does not fail too often (TODO).

## Testing with network clients and servers

```
export MUSE_TEST=FULL_NETW
python3 test.py
```

# Usage

Here's a demo video: https://youtu.be/-H2i-6Dw9vU

There are four types of entities: two cloud service providers, the *data host* and the *query multiplexer*,
and two types of users, *readers* and *writers*.
In practice one physical user can act both as a reader and as a writer,
he just has to run both the reader and the writer software on his machine.

There is a file `muse.parameters.json` at the root of the project.
You may have to edit it to specify the IP addresses and ports of the data host and the query multiplexer.
It is important that all entities have the same `muse.parameters.json` file.

Programs must be launched with this repo as their current working directory
(for me it means that my shell prompt is `~/repos/python-filtered-muse$` when I launch a program).

The datahost must be launched first.
On the datahost machine (and with the correct CWD), enter:

    python3 -m muse.datahost

You should see something like `* Running on [some URL]`.
You can visit the URL to get a web dashboard showing the state of the datahost.

Then, launch the query multiplexer.
It works the same:

    python3 -m muse.querymultiplexer

Now users can use the system.
Users are controlled via a Python shell.
You may want to use the IPython shell (http://ipython.readthedocs.io)
instead of the default Python shell.

On some user machine, let's create a reader account:

    user@readermachine:~/repos/python-filtered-muse$ python3
    Python 3.6.3 (default, Oct  3 2017, 21:45:48) 
    [GCC 7.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> # importing the Reader class that creates readers
    >>> from muse.reader import Reader
    >>> # creating a reader with the name 'reader'
    >>> reader = Reader('reader')
    >>> # now you can go check on the query multiplexer dashboard
    >>> # that the public key for this reader is available
    
Now we will create a writer on some other machine
that will encrypt and upload an index and grant search rights to the reader:

    user@alicemachine:~/repos/python-filtered-muse$ python3
    Python 3.6.3 (default, Oct  3 2017, 21:45:48) 
    [GCC 7.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from muse.writer import Writer
    >>> alice = Writer('Alice')
    >>> # Each line of the index is considered as a keyword
    >>> alice.encrypt('path/to/alice.index.txt')
    >>> # Now you can go check the data host dashboard
    >>> # to check that the index of Alice was uploaded
    >>> alice.delegate('reader')

Back to the reader's shell, we can now search Alice's index

    >>> reader.search('foo')
    ['Alice']
    >>> # this means the keyword 'foo' is present in Alice's index

You can create any number of readers and writers
and of authorizations.
