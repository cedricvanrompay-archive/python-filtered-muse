import unittest
import random
import logging
from random import randint
import os
from types import SimpleNamespace

from muse import core
from muse.pairing import Pairing
import muse.oblivioustransfert as ot
import muse.addhomocipher as ah
from muse.izgbf import IZGBF
from muse.serialization import serialize, deserialize

class SerializationTest(unittest.TestCase):
    def test_serialization_params(self):
        params = core.param_gen()
        b = serialize(params)
        decoded_params = deserialize(b, core.Parameters)
        self.assertEqual(params, decoded_params)

    def test_serialization_authorization(self):
        params = core.param_gen()
        reader = core.Reader(params, 'reader')
        writer = core.Writer(params, 'writer')
        auth = writer.delegate(reader.public_key)

        b = serialize(auth)
        auth2 = deserialize(b, core.Authorization,
                            pairing_group=writer.pairing.group)

        self.assertEqual(auth2, auth)

    def test_serialization_encrypted_index(self):
        params = core.param_gen()

        writer = core.Writer(params, 'writer')
        index = [ os.urandom(8) for _ in range(5) ]
        enc_idx = writer.encrypt(index)

        b = serialize(enc_idx)
        enc_idx_2 = deserialize(b, core.EncryptedIndex,
                                pairing_group=writer.pairing.group)

        self.assertEqual(enc_idx_2, enc_idx)

    def test_serialization_reader_trapdoor(self):
        params = core.param_gen()

        reader = core.Reader(params, 'reader')
        t, xi = reader.trapdoor(b'foo')

        b = serialize(t)
        t_2 = deserialize(b, core.ReaderTrapdoor, pairing_group=reader.pairing.group)

        self.assertEqual(t_2, t)

    def test_serialization_blinding_factor(self):
        params = core.param_gen()

        reader = core.Reader(params, 'reader')
        t, xi = reader.trapdoor(b'foo')

        b = serialize(xi)
        xi_2 = deserialize(b, core.BlindingFactor, pairing_group=reader.pairing.group)

        self.assertEqual(xi_2, xi)


    def test_serialization_transformed_trapdoor(self):
        params = core.param_gen()
        qm = core.QueryMultiplexer(params)
        # FIXME: returns None if target positions are given in a tuple
        # instead of a list
        query = qm.ot_client.query([2,3], db_geometry=(9,5))
        ot_pk = qm.ot_client.public_key
        t = core.TransformedTrapdoor(query, 'fake', 'fake too')

        b = serialize(t)
        t_2 = deserialize(b, core.TransformedTrapdoor,
                          recursive=True, query_list=True,
                          public_key=ot_pk)

        self.assertEqual(t_2, t)

    def test_serialization_response(self):
        params = core.param_gen()
        qm = core.QueryMultiplexer(params)
        encrypt = qm.ot_client.cipher.encrypt
        pk = qm.ot_client.public_key

        response = core.Response(
            ot.OTResponseList([
                ot.RecursiveOTResponse([
                        ot.OTResponse(encrypt(pk, i+j))
                        for j in range(4)
                ])
                for i in range(2)
            ]),
            b'fake enc index id'
        )

        b = serialize(response)
        response_2 = deserialize(b, core.Response, public_key=pk,
                                 recursive=True, response_list=True)

        self.assertEqual(response_2, response)


class PairingsTest(unittest.TestCase):
    def test_determined_by_params(self):
        test_value = os.urandom(8)
        params = core.param_gen()

        results = list()
        for _ in range(2):
            pairing = Pairing(params.curve, params.g1, params.g2)

            e = pairing.group
            h = pairing.h
            g2 = pairing.g2
            pair = pairing.pair

            result = pair(h(test_value), g2)
            results.append(result)

        self.assertEqual(results[0], results[1])

class TestAHCipher(unittest.TestCase):
    def test_basic(self):
        message_byte_size = 256
        cipher = ah.Cipher()
        pk, sk = cipher.keygen()
        max_ptxt = int(pk['n'] - 1)
        msg = randint(max_ptxt//2, max_ptxt)
        ctxt = cipher.encrypt(pk, msg)
        msg_decrypted = cipher.decrypt(pk, sk, ctxt)

        self.assertEqual(msg_decrypted, msg)

    def test_serialization(self):
        cipher = ah.Cipher()
        pk, sk = cipher.keygen()
        msg = randint(1, 2**16-1)
        ctxt = cipher.encrypt(pk, msg)

        bytes = ctxt.to_bytes()
        decoded_ctxt = ah.Ciphertext.from_bytes(bytes, pk)
        self.assertEqual(decoded_ctxt, ctxt)

        bytes = pk.to_bytes()
        decoded_pk = ah.PublicKey.from_bytes(bytes)
        self.assertEqual(decoded_pk, pk)

        decoded_msg = cipher.decrypt(decoded_pk, sk, decoded_ctxt)
        self.assertEqual(decoded_msg, msg)

class IzgbfTest(unittest.TestCase):
    def test_izgbf(self):
        nb_elements = 10
        input_set = list(set([os.urandom(16) for _ in range(nb_elements)]))
        
        izgbf = IZGBF(component_bytesize=8)
        b = izgbf.build(input_set, allow_failures=False)

        value = random.choice(input_set)
        positions = izgbf.map(value, nb_elements)
        shares = [b[i] for i in positions]
        self.assertTrue(izgbf.check(shares), msg="False negative")

        value = os.urandom(16)
        positions = izgbf.map(value, nb_elements)
        shares = [b[i] for i in positions]
        self.assertFalse(izgbf.check(shares), msg="False positive")

class TestOT(unittest.TestCase):
    def setUp(self):
        self.client = ot.Client()
        pub_mat = self.client.get_public_material_bytes()
        self.server = ot.Server(pub_mat)

    def main(self, db_size, component_byte_size, geometry):
        database = [
            os.urandom(component_byte_size)
            for _ in range(db_size)
        ]
        target_position = randint(0, db_size-1)
        encoded_db = self.server.encode(database, geometry)
        q = self.client.query(target_position, geometry or db_size)
        r = self.server.apply(q, encoded_db)
        result = self.client.open(r, component_byte_size)
        self.assertEqual(result, database[target_position])

    def test_basic(self):
        self.main(db_size=16, component_byte_size=8, geometry=None)

    @unittest.skip('TODO fix one day')
    def test_split(self):
        self.main(db_size=4, component_byte_size=512, geometry=None)

    def test_recursive(self):
        self.main(db_size=16, component_byte_size=8, geometry=(4,4))

    def test_non_square(self):
        self.main(db_size=24, component_byte_size=8, geometry=(8,3))

class TestSetup(unittest.TestCase):
    def test_basic(self):
        parameters = core.param_gen()
        writer = core.Writer(parameters, 'index 1')
        reader = core.Reader(parameters, 'reader 1')
        dh = core.DataHost(parameters)
        qm = core.QueryMultiplexer(parameters)

        dh.update_transmission_key(reader.id, reader.transmission_key)

    def test_transmission_key(self):
        parameters = core.param_gen()
        reader1 = core.Reader(parameters, 'reader 1')
        reader2 = core.Reader(parameters, 'reader 2')

        dh = core.DataHost(parameters)

        dh.update_transmission_key(reader1.id, reader1.transmission_key)
        dh.update_transmission_key(reader2.id, reader2.transmission_key)

        reader1.keygen()
        dh.update_transmission_key(reader1.id, reader1.transmission_key)

class IntegrationTest(unittest.TestCase):
    @unittest.skipUnless(os.getenv("MUSE_TEST") == "FULL",
                         "(export MUSE_TEST=FULL to enable)")
    def test_full(self):
        logging.basicConfig(level=logging.INFO)

        logging.info("building database...")

        INDEX_LENGTH = 10

        index = list(set([bytes(randint(0,127) for _ in range(8) )
                 for _ in range(INDEX_LENGTH)
                 ]))

        queried_keyword = random.choice(index)

        logging.info("creating entities...")

        parameters = core.param_gen()
        writer = core.Writer(parameters, 'index 1')
        reader = core.Reader(parameters, 'reader 1')
        dh = core.DataHost(parameters)
        qm = core.QueryMultiplexer(parameters)

        dh.update_transmission_key(reader.id, reader.transmission_key)
        # XXX
        ot_pub_mat = qm.get_ot_public_material()
        dh.set_ot(ot_pub_mat)

        logging.info("executing protocol...")

        encrypted_index = writer.encrypt(index)
        auth = writer.delegate(reader.public_key)
        trapdoor, blinding_factor = reader.trapdoor(queried_keyword)
        transformed_trapdoor = qm.transform(trapdoor, auth, INDEX_LENGTH)
        prepared_index = dh.prepare(encrypted_index, blinding_factor)
        response = dh.process(transformed_trapdoor, prepared_index)
        filtered_response = qm.filter(response)
        result = reader.open(filtered_response) if filtered_response else None

        self.assertEqual(result, writer.index_id)

        logging.info("end.")
    @unittest.skipUnless(os.getenv("MUSE_TEST") == "FULL_SERIAL",
                         "(export MUSE_TEST=FULL_SERIAL to enable)")
    def test_full_serial(self):
        INDEX_LENGTH = 10

        index = list(set([bytes(randint(0,127) for _ in range(8) )
                 for _ in range(INDEX_LENGTH)
                 ]))

        queried_keyword = random.choice(index)

        parameters = core.param_gen()
        writer = core.Writer(parameters, 'index 1')
        reader = core.Reader(parameters, 'reader 1')
        dh = core.DataHost(parameters, {'secparam':512})
        qm = core.QueryMultiplexer(parameters, {'secparam':512})

        dh.update_transmission_key(reader.id, reader.transmission_key)
        ot_pub_mat = qm.get_ot_public_material()
        dh.set_ot(ot_pub_mat)

        encrypted_index = writer.encrypt(index)
        data = serialize(encrypted_index)
        encrypted_index = deserialize(data,
                                      core.EncryptedIndex,
                                      pairing_group=dh.pairing.group)

        auth = writer.delegate(reader.public_key)
        data = serialize(auth)
        auth = deserialize(data, core.Authorization,
                           pairing_group=qm.pairing.group)

        trapdoor, blinding_factor = reader.trapdoor(queried_keyword)

        data = serialize(trapdoor)
        trapdoor = deserialize(data, core.ReaderTrapdoor,
                               pairing_group=qm.pairing.group)

        data = serialize(blinding_factor)
        blinding_factor = deserialize(data,
                                      core.BlindingFactor,
                                      pairing_group=dh.pairing.group) 

        transformed_trapdoor = qm.transform(trapdoor, auth, INDEX_LENGTH)
        data = serialize(transformed_trapdoor)
        transformed_trapdoor = deserialize(data, core.TransformedTrapdoor,
                                           recursive=True, query_list=True,
                                           public_key=dh.ot_server.public_key)

        prepared_index = dh.prepare(encrypted_index, blinding_factor)

        response = dh.process(transformed_trapdoor, prepared_index)
        data = serialize(response)
        response = deserialize(data, core.Response,
                               recursive=True, response_list=True,
                               public_key=qm.ot_client.public_key) 

        filtered_response = qm.filter(response)
        result = reader.open(filtered_response) if filtered_response else None

        self.assertEqual(result, writer.index_id)

        logging.info("end.")

    @unittest.skipUnless(os.getenv("MUSE_TEST") == "FULL_NETW",
                         "(export MUSE_TEST=FULL_NETW to enable)")
    def test_full_with_networking(self):
        import subprocess
        from contextlib import ExitStack
        from time import sleep

        import requests

        from muse.reader import Reader
        from muse.writer import Writer

        def check_servers_are_alive():
            if not (dh_proc.poll() == None and qm_proc.poll() == None):
                raise Exception('A server crashed. Check server logs')

        with ExitStack() as stack:
            try:
                dh_log_file = stack.enter_context(open('/tmp/dh.log', 'w'))
                dh_proc = subprocess.Popen('python3 -m muse.datahost'.split(),
                                         stdout=dh_log_file,
                                         stderr=subprocess.STDOUT)

                qm_log_file = stack.enter_context(open('/tmp/qm.log', 'w'))
                qm_proc = subprocess.Popen('python3 -m muse.querymultiplexer'.split(),
                                         stdout=qm_log_file,
                                         stderr=subprocess.STDOUT)
                
                check_servers_are_alive()

                reader = Reader('reader 1')
                writer = Writer('writer 1')

                writer.encrypt('data/alice.index.txt')
                writer.delegate('reader 1')
                
                with open('data/alice.index.txt') as data_file:
                    keywords = [
                        line.strip()
                        for line in data_file.readlines()
                    ]
                    keyword = random.choice(keywords)
                result = reader.search(keyword)

                self.assertEqual(result, ['writer 1'])
                

                check_servers_are_alive()

            finally:
                dh_proc.kill()
                qm_proc.kill()

if __name__ == "__main__":
    unittest.main(verbosity=2)


