echo "Setup..."

wget https://bootstrap.pypa.io/get-pip.py -P ~/Downloads
sudo python3 ~/Downloads/get-pip.py

if [ ! -e /usr/local/lib/libpbc.so ]; then
    echo "installing PBC"
    sudo apt-get install -y flex bison libgmp-dev

    (
        cd ~/Downloads/
        wget https://crypto.stanford.edu/pbc/files/pbc-0.5.14.tar.gz
        tar -xzf pbc-0.5.14.tar.gz 
        (
            cd pbc-0.5.14/
            ./configure
            make
            sudo make install
        )
    )

    # updates linked libraries
    sudo ldconfig -v &> /tmp/ldconfig.report.txt
fi

if python3 -c "import charm" &> /dev/null ; then
    echo "Charm-crypto already installed"
else
    (
        sudo apt-get install -y python3-dev python3-setuptools libssl-dev
        cd ~/Downloads
        wget https://api.github.com/repos/JHUISI/charm/tarball/7b5faeb -O charm.tar.gz
        tar -xzf charm.tar.gz 
        cd JHUISI-charm-7b5faeb/
        ./configure.sh 
        make
        sudo make install
        sudo make test
    )
fi

sudo pip3 install requests Flask
