import random
import logging
from random import randint
from timeit import timeit
from collections import namedtuple, OrderedDict
import sys

from muse.core import param_gen, Writer, Reader, DataHost, QueryMultiplexer
import muse.oblivioustransfert as ot
from muse.serialization import serialize, deserialize


logging.basicConfig(level=logging.INFO)

class Instruction(namedtuple('Instruction',
                             ['value', 'description',
                              'default_timing_number'])):
    def get_timing(self, number=None, globals=locals()):
        if number == None:
            number = self.default_timing_number

        t = timeit(self.value, number=number, globals=globals)
        return Timing(t/number, 'time for '+self.description, number)

    def execute(self, globals=globals()):
        exec(self.value, globals)
        

class Timing(namedtuple('Timing',
                        ['value', 'description', 'number'])):
    def __str__(self):
        x = self.value
        if x >= 60:
            amount = '{:.3} min'.format(x/60)
        elif x >=0.1:
            amount = '{:.3} s'.format(x)
        elif x >=0.0001:
            amount =  '{:.3} ms'.format(x*1000)
        else:
            amount = '{:.3} us'.format(x*10000)
        return '{}: {}'.format(self.description, amount)

def run():

    logging.info("building database...")

    INDEX_LENGTH = 10

    index = list(set([bytes(randint(0,127) for _ in range(8) )
             for _ in range(INDEX_LENGTH)
             ]))

    queried_keyword = random.choice(index)

    logging.info("creating entities...")

    parameters = param_gen()
    writer = Writer(parameters, 'index 1')
    reader = Reader(parameters, 'reader 1')
    dh = DataHost(parameters)
    qm = QueryMultiplexer(parameters)

    dh.update_transmission_key(reader.id, reader.transmission_key)
    
    ot_pub_mat = qm.get_ot_public_material()
    dh.set_ot(ot_pub_mat)

    logging.info("executing protocol...")

    instructions = OrderedDict()
    instructions['encryption'] = Instruction(
            'encrypted_index = writer.encrypt(index)',
            'encrypting {} keywords'.format(INDEX_LENGTH),
            100,
    )

    instructions['authorization'] = Instruction(
            'auth = writer.delegate(reader.public_key)',
            'creating one authorization',
            100,
    )
    instructions['trapdoor'] = Instruction(
            'trapdoor, blinding_factor = reader.trapdoor(queried_keyword)',
            'creating one trapdoor',
            100,
    )
    instructions['transform'] = Instruction(
            'transformed_trapdoor = qm.transform(trapdoor, auth, %i)' % INDEX_LENGTH,
            'transforming a trapdoor for one index',
            3,
    )
    instructions['prepare'] = Instruction(
            'prepared_index = dh.prepare(encrypted_index, blinding_factor)',
            'preparing one index for query application',
            20,
    )
    instructions['process'] = Instruction(
            'response = dh.process(transformed_trapdoor, prepared_index)',
            'applying one query on one prepared index',
            3,
    )
    instructions['filter'] = Instruction(
            'filtered_response = qm.filter(response)',
            'filtering one response',
            10,
    )
    instructions['open'] = Instruction(
            'result = reader.open(filtered_response)',
            'opening one filtered response',
            100,
    )

    timings = OrderedDict()
    for (key, inst) in instructions.items():
        timings[key] = inst.get_timing(globals=locals())
        inst.execute(globals=locals())
        print(timings[key])

benchmark_size = True
benchmark_timing = True

if len(sys.argv) >= 2:
    if sys.argv[1] == 'timing':
        benchmark_size = False
    elif sys.argv[1] == 'size':
        benchmark_timing = False

if benchmark_size:
    logging.info("building database...")

    INDEX_LENGTH = 10

    index = list(set([bytes(randint(0,127) for _ in range(8) )
             for _ in range(INDEX_LENGTH)
             ]))

    queried_keyword = random.choice(index)

    logging.info("creating entities...")

    parameters = param_gen()
    writer = Writer(parameters, 'index 1')
    reader = Reader(parameters, 'reader 1')
    dh = DataHost(parameters)
    qm = QueryMultiplexer(parameters)

    dh.update_transmission_key(reader.id, reader.transmission_key)
    
    ot_pub_mat = qm.get_ot_public_material()
    dh.set_ot(ot_pub_mat)

    logging.info("executing protocol...")

    encrypted_index = writer.encrypt(index)
    auth = writer.delegate(reader.public_key)
    trapdoor, blinding_factor = reader.trapdoor(queried_keyword)
    transformed_trapdoor = qm.transform(trapdoor, auth, INDEX_LENGTH)
    prepared_index = dh.prepare(encrypted_index, blinding_factor)
    response = dh.process(transformed_trapdoor, prepared_index)
    filtered_response = qm.filter(response)

    for var in ('encrypted_index', 'auth', 'trapdoor',
                'transformed_trapdoor', 'response'):
        size = len(serialize(locals()[var]))
        print('size of serialized', var, ':', size, 'bytes')

if benchmark_timing:
    run()
